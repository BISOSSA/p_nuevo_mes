CREATE OR REPLACE PROCEDURE p_nuevo_mes IS
  v_anio        VARCHAR2(4);
  v_mes         VARCHAR2(2);
  v_semestre_id VARCHAR2(6);
  v_semestre_ds VARCHAR2(20);
  v_nombre_mes  VARCHAR2(20);
  v_nume_mes    VARCHAR2(6);

BEGIN
  SELECT to_char(SYSDATE, 'yyyy')
        ,to_char(SYSDATE, 'mm')
    INTO v_anio
        ,v_mes
    FROM dual;

  v_semestre_ds := CASE
                     WHEN to_char(SYSDATE, 'mm') BETWEEN 1 AND 6 THEN
                      '1er SEMESTRE ' || v_anio
                     ELSE
                      '2do SEMESTRE ' || v_anio
                   END;

  v_semestre_id := CASE
                     WHEN to_char(SYSDATE, 'mm') BETWEEN 1 AND 6 THEN
                      v_anio || '01'
                     ELSE
                      v_anio || '02'
                   END;
  v_nombre_mes  := (to_char(SYSDATE, 'MONTH', 'NLS_DATE_LANGUAGE = SPANISH') || ' ' || v_anio);
  v_nume_mes    := v_anio || v_mes;

  INSERT INTO l_tie_mes
    (id_tie_nume_mes
    ,ds_tie_nomb_mes
    ,id_tie_nume_anio
    ,id_tie_nume_mes_anio
    ,id_tie_semestre
    ,ds_tie_semestre)
  VALUES
    (v_nume_mes
    ,v_nombre_mes
    ,v_anio
    ,trunc(v_mes)
    ,v_semestre_id
    ,v_semestre_ds);
  COMMIT;
END p_nuevo_mes;
/
